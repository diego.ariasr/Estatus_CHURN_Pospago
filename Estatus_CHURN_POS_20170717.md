# Estatus Modelo CHURN PosPago
Diego Arias  
14/7/2017  

## Serie de tiempo de CHURN semanal


<p align="center">
<img src="/grid/home/inpt_red/CHURN_POSPAGO/Serie de tiempo/Graficas/20170717/Serie_tiempo_CHURN_PA_20170717.png">
<div align="center"></div>
</p>

## Efectividad de score (sin clientes repetidos entre cada tabla de entrega)

Se hizo una reingeniería de datos en las variables, metiendo porcentajes de tráfica, voz, contactos, sms, etcétera, como también, el porcentaje de minutos con cabeceras en relación con todas las llamadas entrantes.

Siguientes pasos:

  - Obtener a los exportados por día por medio de un ciclo "FOR" como lo sugirió el área de Retención
  - Minutos y llamadas del cliente a atención a clientes Movistar

Se nota una mejora de la efectividad de CHURN, aunque no es muy significativa.

<p align="center">
<img src="/grid/home/inpt_red/CHURN_POSPAGO/Serie de tiempo/Graficas/20170717/pct_abandono_BD_Semanal_acum_20170717.png">
<div align="center"></div>
</p>

## Clientes identificados de todos los exportados desde la primer entrega

<p align="center">
<img src="/grid/home/inpt_red/CHURN_POSPAGO/Serie de tiempo/Graficas/20170717/bajas_predichas_bdscore_20170717.png">
<div align="center"></div>
</p>


## Clientes identificados de todos los exportados de las tablas de score de Big Data

<p align="center">
<img src="/grid/home/inpt_red/CHURN_POSPAGO/Serie de tiempo/Graficas/20170717/bajas_identificadas_bd_vs_pa_20170717.png">
<div align="center"></div>
</p>
