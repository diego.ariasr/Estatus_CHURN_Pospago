# Estatus Modelo CHURN PosPago
Diego Arias  
3/7/2017  

## Serie de tiempo de CHURN semanal


<p align="center">
<img src="Graficas/20170705/Serie_tiempo_CHURN_PA_20170705.png">
<div align="center"></div>
</p>

## Efectividad de score (sin clientes repetidos entre cada tabla de entrega)

Se observa un caída fuerte en la última entrega, debido a que por la migración de Ónix, en el último TRAIN y PREDICT no se tomaron en cuenta todos los clientes, pues la consulta para obtener el Parque Activo de PosPago cambió. Esto ya se corregirá en la siguiente entrega.

<p align="center">
<img src="Graficas/20170705/pct_abandono_BD_Semanal_acum_20170705.png">
<div align="center"></div>
</p>

## Clientes identificados de todos los exportados desde la primer entrega

<p align="center">
<img src="Graficas/20170705/bajas_predichas_bdscore_20170705.png">
<div align="center"></div>
</p>


## Clientes identificados de todos los exportados de las tablas de score de Big Data

<p align="center">
<img src="Graficas/20170705/bajas_identificadas_bd_vs_pa_20170705.png">
<div align="center"></div>
</p>
